package com.example.UpdatePhoneNumber;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: arthur
 * Date: 8/6/13
 * Time: 11:19 AM
 * To change this template use File | Settings | File Templates.
 */
public class ViewService extends AsyncTask<Void, Void, ArrayList<String>> {

    private ProgressDialog progressDialog;
    private Activity activity;
    private ArrayList<String> arrayList = new ArrayList<String>();
    private Uri uriContact = ContactsContract.Contacts.CONTENT_URI;
    private String contactID;



    @Override
    protected void onPreExecute() {
        super.onPreExecute();    //To change body of overridden methods use File | Settings | File Templates.
        progressDialog = ProgressDialog.show(activity, "Retrieving Contacts Information", "Please wait...");
    }

    @Override
    protected ArrayList<String> doInBackground(Void... voids) {

        Cursor cursor = getContacts();

        int i = 0;

        while (cursor.moveToNext()) {


            String displayName = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
            String phoneNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.HAS_PHONE_NUMBER));
            String contactNumber = retrieveContactNumber(cursor.getString(cursor.getColumnIndex(ContactsContract.Data._ID)));
            arrayList.add(displayName + " : " + phoneNumber + " : " + contactNumber);

        }


        return arrayList;  //To change body of implemented methods use File | Settings | File Templates.
    }


    @Override
    protected void onPostExecute(ArrayList<String> strings) {
        super.onPostExecute(strings);    //To change body of overridden methods use File | Settings | File Templates.

            Intent casesActivity = new Intent(activity, ViewActivity.class);
            casesActivity.putStringArrayListExtra("array",arrayList);
            activity.startActivity(casesActivity);

        progressDialog.dismiss();
    }

    public void setActivityContext(Activity newContext){
        activity = newContext;
    }


    private Cursor getContacts() {
        // Run query
        Uri uri = ContactsContract.Contacts.CONTENT_URI;
        String[] projection = new String[] { ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME, ContactsContract.Contacts.HAS_PHONE_NUMBER};
        String selection = ContactsContract.Contacts.IN_VISIBLE_GROUP + " = '" + ("1") + "'";
        String[] selectionArgs = null;
        String sortOrder = ContactsContract.Contacts.DISPLAY_NAME+ " COLLATE LOCALIZED ASC";
        return activity.managedQuery(uri, projection, selection, selectionArgs, sortOrder);
    }


    private String retrieveContactNumber(String id) {

        String contactNumber = null;

        // getting contacts ID
        Cursor cursorID = activity.getContentResolver().query(uriContact,
                new String[]{ContactsContract.Contacts._ID},
                null, null, null);

        if (cursorID.moveToFirst()) {

            contactID = cursorID.getString(cursorID.getColumnIndex(ContactsContract.Contacts._ID));
        }

        cursorID.close();

        Log.i("Contact ID: ", contactID);

        // Using the contact ID now we will get contact phone number
        Cursor cursorPhone = activity.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},

                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ? AND " +
                        ContactsContract.CommonDataKinds.Phone.TYPE + " = " +
                        ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE,

                new String[]{contactID},
                null);

        if (cursorPhone.moveToFirst()) {
            contactNumber = cursorPhone.getString(cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
        }

        cursorPhone.close();

        Log.i("Contact Phone Number: " ,contactNumber);

        return contactNumber;
    }


}
