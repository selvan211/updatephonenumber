package com.example.UpdatePhoneNumber;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class ViewActivity extends Activity
{
    TextView listView;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view);

        Intent intent = getIntent();
        ArrayList<String> arrayList = intent.getStringArrayListExtra("array");

        listView = (TextView) findViewById(R.id.textView);

        ListView realListView = (ListView) findViewById(R.id.listView);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayList);

        realListView.setAdapter(adapter);

    }

}
