package com.example.UpdatePhoneNumber;

import android.app.Activity;
import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: arthur
 * Date: 8/6/13
 * Time: 11:10 AM
 * To change this template use File | Settings | File Templates.
 */
public class MenuActivity extends Activity {

    private static final int PICK_CONTACT_REQUEST = 1;
    private String mRawContactId;
    private String mDataId;
    private String newPhoneNumber;


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);

        Button viewContacts = (Button) findViewById(R.id.buttonView);
        Button updateContacts = (Button) findViewById(R.id.buttonUpdate);
        Button revertContacts = (Button) findViewById(R.id.buttonRevert);

        viewContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To change body of implemented methods use File | Settings | File Templates.
                ViewService viewService = new ViewService();
                viewService.setActivityContext(MenuActivity.this);
                viewService.execute();

            }
        });

        updateContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To change body of implemented methods use File | Settings | File Templates.
                //updateContactsWithSpecificInformation();

                startActivityForResult(new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI), PICK_CONTACT_REQUEST);

            }
        });


        revertContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To change body of implemented methods use File | Settings | File Templates.

            }
        });
    }


    /***
     *  Update Process without parsing via another Activity
     *
     */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);    //To change body of overridden methods use File | Settings | File Templates.
        if (requestCode == PICK_CONTACT_REQUEST && resultCode == RESULT_OK) {
            //loadContactInfo(data.getData());
            loadContactInfo(data.getData());
        }
    }

    /**
     * Background Thread to load Contact Information
     * @param data
     */
    private void loadContactInfo(Uri data) {
        //To change body of created methods use File | Settings | File Templates.
        AsyncTask<Uri, Void, Void> task = new AsyncTask<Uri, Void, Void>() {

            @Override
            protected Void doInBackground(Uri... uris) {
                Log.v("Retreived ContactURI", uris[0].toString());

                Uri contactUri = uris[0];

                Cursor mContactCursor = getContentResolver().query(contactUri, null, null, null, null);

                try{
                    if(mContactCursor.moveToFirst()){
                        String mContactId = getCursorString(mContactCursor,
                                ContactsContract.Contacts._ID);


                        Cursor mRawContactCursor = getContentResolver().query(
                                ContactsContract.RawContacts.CONTENT_URI,
                                null,
                                ContactsContract.Data.CONTACT_ID + " = ?",
                                new String[] {mContactId},
                                null);



                        Cursor mOtherCursor = getContentResolver().query(
                                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                null,
                                ContactsContract.Data.CONTACT_ID + "= ?",
                                new String[] {mContactId},
                                null);
                        if(mOtherCursor != null){
                                while(mOtherCursor.moveToNext()){
                                    int test2 = mOtherCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                                    String test3 = mOtherCursor.getString(test2);
                                    Log.i("Phone Number",test3);
                                    newPhoneNumber = test3;
                                }


                        }
                        mOtherCursor.close();

                        try {
                            ArrayList<String> mRawContactIds = new ArrayList<String>();
                            while(mRawContactCursor.moveToNext()) {
                                String rawId = getCursorString(mRawContactCursor, ContactsContract.RawContacts._ID);
                                Log.v("RawContact", "ID: " + rawId);
                                mRawContactIds.add(rawId);
                            }

                            for(String rawId : mRawContactIds) {
                                // Make sure the "last checked" RawContactId is set locally for use in insert & update.
                                mRawContactId = rawId;
                                Cursor mDataCursor = getContentResolver().query(
                                        ContactsContract.Data.CONTENT_URI,
                                        null,
                                        ContactsContract.Data.RAW_CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ? AND " + ContactsContract.CommonDataKinds.Phone.TYPE + " = ?",
                                        new String[] { mRawContactId, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, String.valueOf(ContactsContract.CommonDataKinds.Phone.TYPE_HOME)},
                                        null);

                                if(mDataCursor.getCount() > 0) {
                                    mDataCursor.moveToFirst();
                                    mDataId = getCursorString(mDataCursor, ContactsContract.Data._ID);
                                    Log.v("Data", "Found data item with MIMETYPE and PHONE.TYPE");
                                    mDataCursor.close();
                                    break;
                                } else {
                                    Log.v("Data", "Data doesn't contain MIMETYPE and PHONE.TYPE");
                                    mDataCursor.close();
                                }
                            }
                        } finally {
                            mRawContactCursor.close();
                        }
                    }
                }catch (Exception e){
                    Log.w("UpdateContact", e.getMessage());
                    for(StackTraceElement ste : e.getStackTrace()) {
                        Log.w("UpdateContact", "\t" + ste.toString());
                    }
                    throw new RuntimeException();
                } finally {
                    mContactCursor.close();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);    //To change body of overridden methods use File | Settings | File Templates.

                try {
                    ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

                    newPhoneNumber = "5" + newPhoneNumber;

                    ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                            .withValue(ContactsContract.Data.RAW_CONTACT_ID, mRawContactId)
                            .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                            .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, newPhoneNumber)
                            .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_HOME)
                            .build());
                    getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
                    Toast.makeText(getApplicationContext(), "Contact Updated", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    // Display warning
                    Log.w("UpdateContact", e.getMessage());
                    for(StackTraceElement ste : e.getStackTrace()) {
                        Log.w("UpdateContact", "\t" + ste.toString());
                    }
                    Context ctx = getApplicationContext();
                    int duration = Toast.LENGTH_SHORT;
                    Toast.makeText(getApplicationContext(), "Update Fail", Toast.LENGTH_LONG).show();
                }


            }
        };

        task.execute(data);

    }


    /**
     * Returns the String value of the Column index for the specific cursor reference
     * @param cursor
     * @param columnName
     * @return
     */
    private static String getCursorString(Cursor cursor, String columnName) {
        int index = cursor.getColumnIndex(columnName);
        if(index != -1) return cursor.getString(index);
        return null;
    }

}
