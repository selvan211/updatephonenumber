package com.example.UpdatePhoneNumber;

import android.app.Activity;
import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: arthur
 * Date: 8/6/13
 * Time: 3:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class UpdateActivity extends Activity {


    private static final int PICK_CONTACT_REQUEST = 1;
    private String mRawContactId;
    private String mDataId;
    private TextView tResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);    //To change body of overridden methods use File | Settings | File Templates.
        setContentView(R.layout.update);

        Button updateButton = (Button) findViewById(R.id.buttonClick);
        tResult = (TextView) findViewById(R.id.textUpdated);

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To change body of implemented methods use File | Settings | File Templates.

                startActivityForResult(new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI), PICK_CONTACT_REQUEST);
            }
        });

    }

    /**
     * Invoked when the contact picker activity is finished. The {@code contactUri} parameter
     * will contain a reference to the contact selected by the user. We will treat it as
     * an opaque URI and allow the SDK-specific ContactAccessor to handle the URI accordingly.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);    //To change body of overridden methods use File | Settings | File Templates.
        if (requestCode == PICK_CONTACT_REQUEST && resultCode == RESULT_OK) {
            loadContactInfo(data.getData());
        }
    }

    /**
     * Background Thread to load Contact Information
     * @param data
     */
    private void loadContactInfo(Uri data) {
        //To change body of created methods use File | Settings | File Templates.
        AsyncTask<Uri, Void, Void> task = new AsyncTask<Uri, Void, Void>() {

            @Override
            protected Void doInBackground(Uri... uris) {
                Log.v("Retreived ContactURI", uris[0].toString());

                Uri contactUri = uris[0];

                Cursor mContactCursor = getContentResolver().query(contactUri, null, null, null, null);

                try{
                    if(mContactCursor.moveToFirst()){
                        String mContactId = getCursorString(mContactCursor,
                                ContactsContract.Contacts._ID);

                        Cursor mRawContactCursor = getContentResolver().query(
                                ContactsContract.RawContacts.CONTENT_URI,
                                null,
                                ContactsContract.Data.CONTACT_ID + " = ?",
                                new String[] {mContactId},
                                null);
                    try {
                        ArrayList<String> mRawContactIds = new ArrayList<String>();
                        while(mRawContactCursor.moveToNext()) {
                            String rawId = getCursorString(mRawContactCursor, ContactsContract.RawContacts._ID);
                            Log.v("RawContact", "ID: " + rawId);
                            mRawContactIds.add(rawId);
                        }

                        for(String rawId : mRawContactIds) {
                            // Make sure the "last checked" RawContactId is set locally for use in insert & update.
                            mRawContactId = rawId;
                            Cursor mDataCursor = getContentResolver().query(
                                    ContactsContract.Data.CONTENT_URI,
                                    null,
                                    ContactsContract.Data.RAW_CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ? AND " + ContactsContract.CommonDataKinds.Email.TYPE + " = ?",
                                    new String[] { mRawContactId, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE, String.valueOf(ContactsContract.CommonDataKinds.Email.TYPE_HOME)},
                                    null);

                            if(mDataCursor.getCount() > 0) {
                                mDataCursor.moveToFirst();
                                mDataId = getCursorString(mDataCursor, ContactsContract.Data._ID);
                                Log.v("Data", "Found data item with MIMETYPE and EMAIL.TYPE");
                                mDataCursor.close();
                                break;
                            } else {
                                Log.v("Data", "Data doesn't contain MIMETYPE and EMAIL.TYPE");
                                mDataCursor.close();
                            }
                        }
                    } finally {
                        mRawContactCursor.close();
                    }
                  }
                }catch (Exception e){
                    Log.w("UpdateContact", e.getMessage());
                    for(StackTraceElement ste : e.getStackTrace()) {
                        Log.w("UpdateContact", "\t" + ste.toString());
                    }
                    throw new RuntimeException();
                } finally {
                    mContactCursor.close();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);    //To change body of overridden methods use File | Settings | File Templates.

                try {
                    ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

                    ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                            .withValue(ContactsContract.Data.RAW_CONTACT_ID, mRawContactId)
                            .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                            .withValue(ContactsContract.Data.DATA1, "test@test.com")
                            .withValue(ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_HOME)
                            .withValue(ContactsContract.CommonDataKinds.Email.DISPLAY_NAME, "Email")
                            .build());
                    getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
                    tResult.setText("Updated");
                } catch (Exception e) {
                    // Display warning
                    Log.w("UpdateContact", e.getMessage());
                    for(StackTraceElement ste : e.getStackTrace()) {
                        Log.w("UpdateContact", "\t" + ste.toString());
                    }
                    Context ctx = getApplicationContext();
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(ctx, "Update failed", duration);
                    toast.show();
                }


            }
        };

        task.execute(data);

    }


    /**
     * Returns the String value of the Column index for the specific cursor reference
     * @param cursor
     * @param columnName
     * @return
     */
    private static String getCursorString(Cursor cursor, String columnName) {
        int index = cursor.getColumnIndex(columnName);
        if(index != -1) return cursor.getString(index);
        return null;
    }


}
